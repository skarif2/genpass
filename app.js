let express = require('express'),
    generatePassword = require("password-generator"),
    app = express();

/*======================== Method-1 (password-generator) =====================*/
// using random password-generator
let uprMinCount = 2,
    lwrMinCount = 4,
    numMinCount = 1
    splMinCount = 1,
    UPPERCASE_RE = /([A-Z])/g,
    LOWERCASE_RE = /([a-z])/g,
    NUMBER_RE = /([\d])/g,
    SPECIAL_CHAR_RE = /([\?\-])/g,
    NON_REPEATING_CHAR_RE = /([\w\d\?\-])\1{2,}/g;

function isStrongEnough(password) {
  let uc = password.match(UPPERCASE_RE),
      lc = password.match(LOWERCASE_RE),
      n = password.match(NUMBER_RE),
      sc = password.match(SPECIAL_CHAR_RE),
      nr = password.match(NON_REPEATING_CHAR_RE);
  return password.length === uprMinCount+lwrMinCount+numMinCount+splMinCount &&
    !nr &&
    uc && uc.length === uprMinCount &&
    lc && lc.length === lwrMinCount &&
    n && n.length === numMinCount &&
    sc && sc.length === splMinCount;
}

// main function where you will get random password using password-generator module
function customPassword() {
  let password = "";
  let randomLength = uprMinCount + lwrMinCount + numMinCount + splMinCount;
  let count = 0;
  while (!isStrongEnough(password)) {
    password = generatePassword(randomLength, false, /[\d\W\w\p]/);
    count += 1;
  }
  console.log(count);
  return password;
}
/*=============================== end of Method-1 ============================*/


/*============================ Method-2(own thoughts) ========================*/
let parCount = 0;

// get random characters from a set of characters(charSet) on a prefered length(prefLength)
function getRandom(charSet, prefLength) {
    let text = "";
    let possible = charSet;
    for( var i=0; i < prefLength; i++ ) {
      parCount += 1;
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

// unbiased shuffle algorithm - the Fisher-Yates (aka Knuth) Shuffle
function shuffle(value) {
  let array = value.split(' ');
  let currentIndex = array.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    parCount += 1;
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array.join('');
}

// main function where you will get password using my thoughts
function anotherCustomPass() {
  const uprChar = getRandom("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 2);
  const lwrChar = getRandom("abcdefghijklmnopqrstuvwxyz", 4);
  const numChar = getRandom("0123456789", 1);
  const splChar = getRandom("!#$%&*+?@", 1);

  const genPass = shuffle(`${uprChar} ${lwrChar} ${numChar} ${splChar}`);
  console.log(parCount);
  parCount = 0;
  return genPass;
}
/*=============================== end of Method-2 ============================*/

// base api localhost:8080 - does nothing, just shows "try /genpass" on screen
app.get('/', function(req, res) {
  res.send('try /genpass');
});

// api localhost:8080/genpass - shows random passwords which has a length of 8 using both methods
app.get('/genpass', function(req, res){
  const randomPassword = customPassword();
  const randomPassword2 = anotherCustomPass();
  res.json({
    'Password_1': randomPassword,
    'Password_2': randomPassword2
  });
});

app.listen(8080);
